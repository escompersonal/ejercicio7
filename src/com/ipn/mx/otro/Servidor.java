/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipn.mx.otro;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author carlos
 */
public class Servidor extends Thread {

    public static final int PUERTO = 6666;
    ServerSocket socketServidor;

    public Servidor() {
        try {
            socketServidor = new ServerSocket(PUERTO);
            System.out.println("Funcionando");
        } catch (IOException e) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, e);
        }
        start();
    }

    @Override
    public void run() {
        while(true){
            try {
                Socket socketCliente = socketServidor.accept();
                SQLConexion miConexion = new SQLConexion(socketCliente);
                
            } catch (IOException ex) {
                Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void main(String[] args) {
        new Servidor();
    }
    

}
