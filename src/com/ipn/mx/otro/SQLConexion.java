/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipn.mx.otro;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author carlos
 */
public class SQLConexion extends Thread {

    protected Socket socketCliente;
    protected BufferedReader entrada;
    protected PrintStream salida;
    protected String consulta;

    public SQLConexion(Socket socketCliente) {
        this.socketCliente = socketCliente;
        try {
            entrada = new BufferedReader(new InputStreamReader(this.socketCliente.getInputStream()));
            salida = new PrintStream(this.socketCliente.getOutputStream());
        } catch (IOException e) {
            System.err.println(e);
            try {
                this.socketCliente.close();
            } catch (IOException ex) {
            }
            return;
        }
        start();
    }

    @Override
    public void run() {
        try {
            consulta = entrada.readLine();
            System.err.println("Consulta a Ejecutar: " + consulta + ";");
            ejecutaSQL();
        } catch (IOException e) {
        } finally {
            try {
                socketCliente.close();
            } catch (IOException ex) {
            }
        }
    }

    private void ejecutaSQL() {
        Connection cnn;
        Statement st;
        ResultSet rs;
        ResultSetMetaData resultadoMetaData;
        boolean existenMasFilas;
        String driver = "com.mysql.jdbc.Driver";
        String usuario = "root", clave = "root", registro;
        int numeroColumnas, i;
        try {
            Class.forName(driver);
            cnn = DriverManager.getConnection("jdbc:mysql://localhost:3306/Distribuidos", usuario, clave);
            st = cnn.createStatement();
            rs = st.executeQuery(consulta);
            existenMasFilas = rs.next();
            if (!existenMasFilas) {
                salida.println("No hay mas filas");
                return;
            }
            resultadoMetaData = rs.getMetaData();
            numeroColumnas = resultadoMetaData.getColumnCount();
            System.out.println(numeroColumnas + " columnas en el resultado.");
            while (existenMasFilas) {
                registro = "";
                for (i = 1; i <= numeroColumnas; i++) {
                    registro = registro.concat(rs.getString(i) + " ");
                }
                salida.println(registro);
                System.out.println(registro);
                existenMasFilas = rs.next();
            }
            rs.close();
            st.close();
            cnn.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e);
            e.printStackTrace();
        }

    }

}
